// Peakmeter.cpp -- WinMain and dialog box functions

#include <windows.h>
#include <mmdeviceapi.h>
#include <endpointvolume.h>
#include "resource.h"
#include <iostream>
#include <fstream>
#include <Audioclient.h>
#include <Mmreg.h>
#include <gdiplus.h>
#include <gdiplusgraphics.h>

VOID CALLBACK asdf(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam);
static BOOL CALLBACK DlgProc(HWND, UINT, WPARAM, LPARAM);
static void DrawPeakMeter(HWND, float);

// Timer ID and period (in milliseconds)
#define ID_TIMER  1
#define TIMER_PERIOD  25

#define EXIT_ON_ERROR(hr)  \
if (FAILED(hr)) { goto Exit; }
#define SAFE_RELEASE(punk)  \
if ((punk) != NULL)  \
{ (punk)->Release(); (punk) = NULL; }


//-----------------------------------------------------------
// Record an audio stream from the default audio capture
// device. The RecordAudioStream function allocates a shared
// buffer big enough to hold one second of PCM audio data.
// The function uses this buffer to stream data from the
// capture device. The main loop runs every 1/2 second.
//-----------------------------------------------------------

// REFERENCE_TIME time units per second and per millisecond
#define REFTIMES_PER_SEC  10000000
#define REFTIMES_PER_MILLISEC  10000

#define EXIT_ON_ERROR(hres)  \
if (FAILED(hres)) { goto Exit; }
#define SAFE_RELEASE(punk)  \
if ((punk) != NULL)  \
{ (punk)->Release(); (punk) = NULL; }

const CLSID CLSID_MMDeviceEnumerator = __uuidof(MMDeviceEnumerator);
const IID IID_IMMDeviceEnumerator = __uuidof(IMMDeviceEnumerator);
const IID IID_IAudioClient = __uuidof(IAudioClient);
const IID IID_IAudioCaptureClient = __uuidof(IAudioCaptureClient);

#define bufferSize 1000
#define displaySamples 400

class MyAudioSink
{
public:
	HRESULT CopyData(BYTE* pData, UINT32 numFrames, BOOL* bDone);
	HRESULT SetFormat(WAVEFORMATEX* pwfx);
	void openFile(void);
	void closeFile(void);
	void MyAudioSink::printBuffer(void);
	void startNewBuffer(void);
private:
	WAVEFORMATEX format;
	std::ofstream myFile;
	float dataBuffer[bufferSize*2];
	int bytesRead;
	int findCenter(void);

	HPEN pen = CreatePen(PS_SOLID, 2, RGB(255, 0, 0));
	HPEN greenPen = CreatePen(PS_SOLID, 2, RGB(0, 255, 0));

	HRGN screen = CreateRectRgn(0, 0, 1024, 500);
	HBRUSH clear = CreateSolidBrush(RGB(0, 0, 0));

	HWND console_handle = GetConsoleWindow();
	HDC device_context = GetDC(console_handle);

	float getMax(float*buffer, int count);

};

float MyAudioSink::getMax(float*buffer, int count)
{
	float max;
	max = buffer[count];
	for (count-=2; count > 0; count-=2)
	{
		if (buffer[count] > max)
		{
			max = buffer[count];
		}
	}
	return max;
}

void MyAudioSink::startNewBuffer(void)
{
	bytesRead = 0;
	return;
}

void MyAudioSink::closeFile(void)
{
//	free(dataBuffer);
//	myFile.close();
}

void MyAudioSink::openFile(void)
{
//	myFile.open("test.txt", std::fstream::binary | std::ios::out);
//	dataBuffer = (char*)malloc(bufferSize);
}

int MyAudioSink::findCenter(void)
{
	int i;
	int negCount = 0, posCount = 0;
	int maxCount, sumCount;
	float sum;
	
	// Center must have at least 1/2 of the display size on each side
	for (maxCount = 10; maxCount >= 0; maxCount--)
	{
		negCount = 0;
		posCount = 0;
		for (i = displaySamples / 2; i < bufferSize - displaySamples / 2; i++)
		{
			sum = dataBuffer[i * 2];
			for (sumCount = 0; sumCount < maxCount; sumCount++)
			{
				sum += dataBuffer[i * 2 - sumCount * 2];
			}

			if (negCount >= maxCount)
			{
				if (sum >= 0)
				{
					posCount++;
					if (posCount >= maxCount)
					{
						return i;
					}
				}
				else
				{
					negCount = 0;
					posCount = 0;
				}
			}
			else
			{
				if (sum < 0)
				{
					negCount++;
				}
				else
				{
					negCount = 0;
				}
			}
		}
	}
	return bufferSize/2; // no rising edge found, return middle of buffer
}


HRESULT MyAudioSink::SetFormat(WAVEFORMATEX* ppwfx)
{
	format = *ppwfx;
	std::cout << "Channels: " << format.nChannels << "\n"
		<< "Samples/sec: " << format.nSamplesPerSec << "\n"
		<< "Bits/sample: " << format.wBitsPerSample << "\n"
		<< "Block Size: " << format.nBlockAlign << "\n";
	return 1;
}

void MyAudioSink::printBuffer(void)
{
	int row, col;
	int center;
	float max;

	//Here's a 5 pixels wide RED line [from initial 0,0] to 300,300
	MoveToEx(device_context, 0, 250, NULL);

	center = findCenter();

	//dataBuffer[center*2] = 1;
	//dataBuffer[center*2 + 2] = -1;
	//dataBuffer[center * 2 + 4] = 1;

	FillRgn(device_context, screen, clear);

//	max = getMax(&dataBuffer[center * 2 - displaySamples], displaySamples);

	// Loop from -1/2 to 1/2 samples around the center
	for (col = -displaySamples/2; col < displaySamples/2; col++)
	{
		SelectObject(device_context, greenPen);
		// actual x column goes from 0 to displaySamples (add displaySamples/2)
		LineTo(device_context, col+displaySamples/2, (int)(dataBuffer[center*2 + col*2] * 100.0) + 250);
	}

}

HRESULT MyAudioSink::CopyData(BYTE* pData, UINT32 numFrames, BOOL* bDone)
{
	int i;

	if (pData == NULL)
	{
		std::cout << "Silence of the lambs\n";
	}
	else
	{
		if (*bDone == FALSE)
		{
			for (i = 0; i < numFrames*2 && bytesRead < bufferSize * 2; i++)
			{
				dataBuffer[bytesRead] = ((float*)pData)[i];
				bytesRead++;
			}

			if (bytesRead == bufferSize * 2)
			{
				printBuffer();
				bytesRead++; // only call this once
//				*bDone = TRUE;
			}
		}
	}
	return 1;
}

HRESULT RecordAudioStream(MyAudioSink *pMySink)
{
	HRESULT hr;
	REFERENCE_TIME hnsRequestedDuration = REFTIMES_PER_SEC;
	REFERENCE_TIME hnsActualDuration;
	UINT32 bufferFrameCount;
	UINT32 numFramesAvailable;
	IMMDeviceEnumerator *pEnumerator = NULL;
	IMMDevice *pDevice = NULL;
	IAudioClient *pAudioClient = NULL;
	IAudioCaptureClient *pCaptureClient = NULL;
	WAVEFORMATEX *pwfx = NULL;
	UINT32 packetLength = 0;
	BOOL bDone = FALSE;
	BYTE *pData;
	DWORD flags;

	hr = CoCreateInstance(
		CLSID_MMDeviceEnumerator, NULL,
		CLSCTX_ALL, IID_IMMDeviceEnumerator,
		(void**)&pEnumerator);
	EXIT_ON_ERROR(hr)

		hr = pEnumerator->GetDefaultAudioEndpoint(
		eCapture, eConsole, &pDevice);
	EXIT_ON_ERROR(hr)

		hr = pDevice->Activate(
		IID_IAudioClient, CLSCTX_ALL,
		NULL, (void**)&pAudioClient);
	EXIT_ON_ERROR(hr)

		hr = pAudioClient->GetMixFormat(&pwfx);
	EXIT_ON_ERROR(hr)

		hr = pAudioClient->Initialize(
		AUDCLNT_SHAREMODE_SHARED,
		0,
		hnsRequestedDuration,
		0,
		pwfx,
		NULL);
	EXIT_ON_ERROR(hr)

		// Get the size of the allocated buffer.
		hr = pAudioClient->GetBufferSize(&bufferFrameCount);
	EXIT_ON_ERROR(hr)

		hr = pAudioClient->GetService(
		IID_IAudioCaptureClient,
		(void**)&pCaptureClient);
	EXIT_ON_ERROR(hr)

		// Notify the audio sink which format to use.
		hr = pMySink->SetFormat(pwfx);
	EXIT_ON_ERROR(hr)

		// Calculate the actual duration of the allocated buffer.
		hnsActualDuration = (double)REFTIMES_PER_SEC *
		bufferFrameCount / pwfx->nSamplesPerSec;

	hr = pAudioClient->Start();  // Start recording.
	EXIT_ON_ERROR(hr)

		// Each loop fills about half of the shared buffer.
	while (bDone == FALSE)
	{
	//	Sleep(hnsActualDuration / REFTIMES_PER_MILLISEC / 2);
		pMySink->startNewBuffer();
		// Adjust to be enough to fill the buffer, but short enough to maintain reasonable refresh rate
		Sleep(23);
		hr = pCaptureClient->GetNextPacketSize(&packetLength);
		EXIT_ON_ERROR(hr)

		while (packetLength != 0)
		{
			// Get the available data in the shared buffer.
			hr = pCaptureClient->GetBuffer(
				&pData,
				&numFramesAvailable,
				&flags, NULL, NULL);
			EXIT_ON_ERROR(hr)

			if (flags & AUDCLNT_BUFFERFLAGS_SILENT)
			{
				pData = NULL;  // Tell CopyData to write silence.
			}

			// Copy the available capture data to the audio sink.
			hr = pMySink->CopyData(
				pData, numFramesAvailable, &bDone);
			EXIT_ON_ERROR(hr)

				hr = pCaptureClient->ReleaseBuffer(numFramesAvailable);
			EXIT_ON_ERROR(hr)

				hr = pCaptureClient->GetNextPacketSize(&packetLength);
			EXIT_ON_ERROR(hr)
		}

	}

	hr = pAudioClient->Stop();  // Stop recording.
	EXIT_ON_ERROR(hr)

	Exit:
	CoTaskMemFree(pwfx);
	SAFE_RELEASE(pEnumerator)
		SAFE_RELEASE(pDevice)
		SAFE_RELEASE(pAudioClient)
		SAFE_RELEASE(pCaptureClient)

		return hr;
}


//-----------------------------------------------------------
// asdf -- Opens a dialog box that contains a peak meter.
//   The peak meter displays the peak sample value that plays
//   through the default rendering device.
//-----------------------------------------------------------
int main()
{
	HRESULT hr;
	HRESULT hr2;
	IMMDeviceEnumerator *pEnumerator = NULL;
	IMMDevice *pDevice = NULL;
	IAudioMeterInformation *pMeterInfo = NULL;
	static float peak = 0;

	int count;

	BOOL bRet;
	MSG msg;

	MyAudioSink sinky;

	CoInitialize(NULL);

	// Get enumerator for audio endpoint devices.
	hr = CoCreateInstance(__uuidof(MMDeviceEnumerator),
		NULL, CLSCTX_INPROC_SERVER,
		__uuidof(IMMDeviceEnumerator),
		(void**)&pEnumerator);
	EXIT_ON_ERROR(hr)

		// Get peak meter for default audio-rendering device.
		hr = pEnumerator->GetDefaultAudioEndpoint(eRender, eConsole, &pDevice);
	EXIT_ON_ERROR(hr)

		hr = pDevice->Activate(__uuidof(IAudioMeterInformation),
		CLSCTX_ALL, NULL, (void**)&pMeterInfo);
	EXIT_ON_ERROR(hr)

		//	SetTimer(NULL, ID_TIMER, TIMER_PERIOD, NULL);
		sinky.openFile();
		RecordAudioStream(&sinky);
		sinky.closeFile();

	while ((bRet = GetMessage(&msg, NULL, 0, 0)) != 0)
	{
		if (bRet == -1)
		{
			EXIT_ON_ERROR(hr);
		}
		else
		{
			if (msg.message == WM_TIMER)
			{
				//				for (count = 0; count <= (int)(peak*100); count++)
				//					std::cout << "|";
				//				std::cout << "\n";
				std::cout << (int)(peak * 255) << "\n" << std::flush;
				hr2 = pMeterInfo->GetPeakValue(&peak);

				if (FAILED(hr))
				{
					KillTimer(NULL, ID_TIMER);
					return 0;
				}
			}
			DispatchMessage(&msg);
		}
	}

Exit:
	if (FAILED(hr))
	{
		MessageBox(NULL, TEXT("This program requires Windows Vista."),
			TEXT("Error termination"), MB_OK);
	}
	SAFE_RELEASE(pEnumerator)
		SAFE_RELEASE(pDevice)
		SAFE_RELEASE(pMeterInfo)
		CoUninitialize();
	return 0;
}