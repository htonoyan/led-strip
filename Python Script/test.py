import sys
import serial
import time
import random

ser = serial.Serial()
ser.port = 3
ser.baudrate = 250000
ser.bytesize = serial.EIGHTBITS
ser.parity = serial.PARITY_EVEN
ser.stopbits = serial.STOPBITS_TWO
#ser.open()

class PixelWriter(object):
    def __init__(self, serial):
        self.serial = serial

    def write(self, pixels):
        # TODO: replace this homebrew packing
        frame = [0xAA, len(pixels)*3 & 0xFF, (len(pixels)*3 & 0xFF00) >> 8]
        for pixel in pixels:
            frame.extend(pixel)

        self.serial.write(frame)


class STDOUTDebugPixelWriter(object):
    def write(self, pixels):
        print "DOING WRITE WITH", len(pixels)
        for i, pixel in enumerate(pixels):
            print "PIXEL", i, pixel

writer = STDOUTDebugPixelWriter()

def peaks():
    while 1:
        yield int(sys.stdin.readline())

numPixels = 10

brightness = 255
glowRange = 30

pixelColors = []

for i in range(1,numPixels+1):
    if i < numPixels / 4.0 :
        pixelColors.append([i*255*4/numPixels, 0, 255])
    elif i < numPixels / 2.0 :
        pixelColors.append([255, 0, 510 - i*255*4/numPixels])
    elif i < numPixels * 3.0 / 4.0 :
        pixelColors.append([255, i*255*4/numPixels - 510, 0])
    else:
        pixelColors.append([1020 - i*255*4/numPixels, 255, 0])

for peak in peaks():
    pixels = []

    # style 1
    for i in range(1,numPixels+1):
        if i <= peak*numPixels/255: # only display pixels that are below the "peak"
            pixels.append([int(x*brightness/255) for x in pixelColors[i-1]])
        else:
            if i <= (peak+glowRange)*numPixels/255:
                pixels.append([int(x*brightness/255.0 - x*brightness*(i*255.0/numPixels - peak)/glowRange/255.0) for x in pixelColors[i-1]])
            else:
                pixels.append([0, 0, 0])

            # Style 2
            # if peak < 255 / 4.0 :
                # frame.extend([peak*4, 0, 255])
            # elif peak < 255 / 2.0 :
                # frame.extend([255, 0, 510 - peak*4])
            # elif peak < 255 * 3.0 / 4.0 :
                # frame.extend([255, peak*4 - 510, 0])
            # else:
                # frame.extend([1020 - peak*4, 255, 0])
                
            # Style 3
             # if peak < 255 / 4.0 :
                # frame.extend([peak*4, 0, 255])
            # elif peak < 255 / 2.0 :
                # frame.extend([255, 0, 510 - peak*4])
            # elif peak < 255 * 3.0 / 4.0 :
                # frame.extend([255, peak*4 - 510, 0])
            # else:
                # frame.extend([1020 - peak*4, 255, 0])

#    print(frame)
    print(peak)
    writer.write(pixels)