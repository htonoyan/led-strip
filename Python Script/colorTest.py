numPixels = 10

brightness = 255
glowRange = 30

pixelColors = []

for i in range(1,numPixels+1):
    if i < numPixels / 4.0 :
        pixelColors.append([i*255*4/numPixels, 0, 255])
    elif i < numPixels / 2.0 :
        pixelColors.append([255, 0, 510 - i*255*4/numPixels])
    elif i < numPixels * 3.0 / 4.0 :
        pixelColors.append([255, i*255*4/numPixels - 510, 0])
    else:
        pixelColors.append([1020 - i*255*4/numPixels, 255, 0])

for peak in range(0,30):
    frame = []
    # style 1
    for i in range(1,numPixels+1):
        if i <= peak*numPixels/255: # only display pixels that are below the "peak"
            frame.extend(int(x*brightness/255) for x in pixelColors[i-1])
            print( peak, i, [int(x*brightness/255) for x in pixelColors[i-1] ])
        else:
            if i <= (peak+glowRange)*numPixels/255:
                frame.extend(int(x*brightness/255.0 - x*brightness*(i*255.0/numPixels - peak)/glowRange/255.0) for x in pixelColors[i-1])
                print( peak, i, [int(x*brightness/255.0 - x*brightness*(i*255.0/numPixels - peak)/glowRange/255.0) for x in pixelColors[i-1] ])
            else:
                frame.extend([0, 0, 0])